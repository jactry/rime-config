#!/usr/bin/env bash

LUAN_PINYIN="luna_pinyin.dict.yaml"
LUAN_PINYIN_URL="https://raw.githubusercontent.com/rime/rime-luna-pinyin/master/luna_pinyin.dict.yaml"

LUAN_PINYIN_POETRY="luna_pinyin.poetry.dict.yaml"
LUAN_PINYIN_POETRY_URL="https://raw.githubusercontent.com/rime-aca/dictionaries/master/luna_pinyin.dict/luna_pinyin.poetry.dict.yaml"

RIME_JAPANESE_DICT="japanese.dict.yaml"
RIME_JAPANESE_DICT_URL="https://raw.githubusercontent.com/gkovacs/rime-japanese/master/japanese.dict.yaml"
RIME_JAPANESE_SCHEMA="japanese.schema.yaml"
RIME_JAPANESE_SCHEMA_URL="https://raw.githubusercontent.com/gkovacs/rime-japanese/master/japanese.schema.yaml"
RIME_JAPANESE_JMDICT="japanese.jmdict.dict.yaml"
RIME_JAPANESE_JMDICT_URL="https://raw.githubusercontent.com/gkovacs/rime-japanese/master/japanese.jmdict.dict.yaml"
RIME_JAPANESE_KANA="japanese.kana.dict.yaml"
RIME_JAPANESE_KANA_URL="https://raw.githubusercontent.com/gkovacs/rime-japanese/master/japanese.kana.dict.yaml"
RIME_JAPANESE_MOZC="japanese.mozc.dict.yaml"
RIME_JAPANESE_MOZC_URL="https://raw.githubusercontent.com/gkovacs/rime-japanese/master/japanese.mozc.dict.yaml"

RIME_EMOJI_OPENCC_PATH="opencc"
RIME_EMOJI_OPENCC_CATEGORY=${RIME_EMOJI_OPENCC_PATH}"/emoji_category.txt"
RIME_EMOJI_OPENCC_CATEGORY_URL="https://raw.githubusercontent.com/rime/rime-emoji/master/opencc/emoji_category.txt"
RIME_EMOJI_OPENCC_JSON=${RIME_EMOJI_OPENCC_PATH}"/emoji.json"
RIME_EMOJI_OPENCC_JSON_URL="https://raw.githubusercontent.com/rime/rime-emoji/master/opencc/emoji.json"
RIME_EMOJI_OPENCC_WORD=${RIME_EMOJI_OPENCC_PATH}"/emoji_word.txt"
RIME_EMOJI_OPENCC_WORD_URL="https://raw.githubusercontent.com/rime/rime-emoji/master/opencc/emoji_word.txt"

SOGOU_SELECTED="luna_pinyin.sogou.selected.dict.yaml"
SOGOU_SELECTED_TXT="sogou_selected.txt"
SOGOU_SELECTED_SCEL="sogou_selected.scel"
SOGOU_SELECTED_URL="https://pinyin.sogou.com/d/dict/download_cell.php?id=11826&name=%E6%90%9C%E7%8B%97%E7%B2%BE%E9%80%89%E8%AF%8D%E5%BA%93&f=detail"

SOGOU_POPULAR="luna_pinyin.sogou.popular.dict.yaml"
SOGOU_POPULAR_TXT="sogou_popular.txt"
SOGOU_POPULAR_SCEL="sogou_popular.scel"
SOGOU_POPULAR_URL="https://pinyin.sogou.com/d/dict/download_cell.php?id=4&name=%E7%BD%91%E7%BB%9C%E6%B5%81%E8%A1%8C%E6%96%B0%E8%AF%8D%E3%80%90%E5%AE%98%E6%96%B9%E6%8E%A8%E8%8D%90%E3%80%91&f=detail"

WIKIZH="luna_pinyin.zhwiki.dict.yaml"
WIKIZH_ZIP="luna_pinyin.zhwiki.dict.zip"
WIKIZH_URL="https://github.com/jactry/luna_pinyin.zhwiki.dict/releases/download/2024030101/luna_pinyin.zhwiki.dict.zip"

TMP_PATH="/tmp/`date +%s`"
mkdir ${TMP_PATH}

download()
{
    echo "Downloading $1 to $2 ..."
    wget -c -t 3 $1 -O $2
    if [ $? -ne 0 ]; then
        "Failed to download $1 ."
    fi
}

txt2dict()
{
    YAML_DICT_HEADER="---\nname: $3\nversion: "`date +\"%Y.%m.%d\"`"\nsort: by_weight\nuse_preset_vocabulary: true\n...\n"

    echo -ne ${YAML_DICT_HEADER} >> $2
    cat $1 >> $2
}

check()
{
    if [ $? -eq 0 ]; then
        echo -e "\033[32mUpdated $1 .\033[0m"
    else
        echo -e "\033[31mFailed to update $1 .\033[0m"
    fi
}

update_scel_dict()
{
    download $1 "${TMP_PATH}/${2}"
    if [ $? -eq 0 ]; then
        ./imewlconverter/ImeWlConverterCmd -i:scel "${TMP_PATH}/${2}" -o:rime "${TMP_PATH}/${3}" -ct:pinyin -os:linux
        txt2dict "${TMP_PATH}/${3}" "${TMP_PATH}/Hans_${3}" $4
        if [ $? -eq 0 ]; then
            opencc -i "${TMP_PATH}/Hans_${3}" -o $4
        fi
    fi
}

update_rime_emoji()
{
    mkdir ${RIME_EMOJI_OPENCC_PATH}
    download ${RIME_EMOJI_OPENCC_CATEGORY_URL} ${RIME_EMOJI_OPENCC_CATEGORY}
    check ${RIME_EMOJI_OPENCC_CATEGORY}
    download ${RIME_EMOJI_OPENCC_JSON_URL} ${RIME_EMOJI_OPENCC_JSON}
    check ${RIME_EMOJI_OPENCC_JSON}
    download ${RIME_EMOJI_OPENCC_WORD_URL} ${RIME_EMOJI_OPENCC_WORD}
    check ${RIME_EMOJI_OPENCC_WORD}
    mv -f ${RIME_EMOJI_OPENCC_PATH}/* ../${RIME_EMOJI_OPENCC_PATH}/
}

update()
{
    download ${LUAN_PINYIN_URL} ${LUAN_PINYIN}
    check ${LUAN_PINYIN}
    download ${LUAN_PINYIN_POETRY_URL} ${LUAN_PINYIN_POETRY}
    check ${LUAN_PINYIN_POETRY}
    download ${RIME_JAPANESE_DICT_URL} ${RIME_JAPANESE_DICT}
    check ${RIME_JAPANESE_DICT}
    download ${RIME_JAPANESE_SCHEMA_URL} ${RIME_JAPANESE_SCHEMA}
    check ${RIME_JAPANESE_SCHEMA}
    download ${RIME_JAPANESE_JMDICT_URL} ${RIME_JAPANESE_JMDICT}
    check ${RIME_JAPANESE_JMDICT}
    download ${RIME_JAPANESE_KANA_URL} ${RIME_JAPANESE_KANA}
    check ${RIME_JAPANESE_KANA}
    download ${RIME_JAPANESE_MOZC_URL} ${RIME_JAPANESE_MOZC}
    check ${RIME_JAPANESE_MOZC}

    update_rime_emoji

    update_scel_dict ${SOGOU_SELECTED_URL} ${SOGOU_SELECTED_SCEL} ${SOGOU_SELECTED_TXT} ${SOGOU_SELECTED}
    check ${SOGOU_SELECTED}

    update_scel_dict ${SOGOU_POPULAR_URL} ${SOGOU_POPULAR_SCEL} ${SOGOU_POPULAR_TXT} ${SOGOU_POPULAR}
    check ${SOGOU_POPULAR}

    download ${WIKIZH_URL} ${WIKIZH_ZIP}
    unzip ${WIKIZH_ZIP}

    mv *.yaml ..
}

update
