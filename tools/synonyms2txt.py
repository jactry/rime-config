import sys
import re
from pypinyin import lazy_pinyin, load_phrases_dict

rows = open(sys.argv[1], "r").readlines()
regex = re.compile(r"([^,]+) => ([^,]+), (.+)")
drain_colors = ["🏻", "🏻", "🏻", "🏻", "🏼", "🏼", "🏼", "🏼", "🏽", "🏽", "🏽", "🏽", "🏾", "🏾", "🏾", "🏾", "🏿", "🏿", "🏿", "🏿"]

# fix heteronym
load_phrases_dict({
  "弹奏": [["tán"], ["zòu"]]
})

target_file = open(sys.argv[2], "w+", encoding="utf8")
for row in rows:
    match = regex.match(row)
    emoji, _, alias_matched = match.groups()
    if [x for x in drain_colors if x in emoji]:
        continue
    aliases = filter(lambda x: ":" not in x and "-" not in x and "肤色" not in x,
                     map(lambda x: x.replace("旗: ", "").strip().lower(),
                         alias_matched.split(",")))
    for alias in aliases:
        pinyins = lazy_pinyin(alias)
        if len(pinyins) == 1 and len(pinyins[0]) == 1:
            continue
        target_file.write("\n%s\t%s\t1" % (emoji, " ".join(pinyins)))
